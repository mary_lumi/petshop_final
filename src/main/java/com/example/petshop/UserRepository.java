package com.example.petshop;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>{

    // auto generate the SQL!!!
    User findByUsername(String username);
}

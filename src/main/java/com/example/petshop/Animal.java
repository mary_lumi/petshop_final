package com.example.petshop;

public class Animal {
    private Integer ID_Animal;

    private String Nume;
    private String Talie;
    private String Rasa;

    public Integer getID_Animal() {
        return ID_Animal;
    }

    public void setID_Animal(Integer ID_Animal) {
        this.ID_Animal = ID_Animal;
    }

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public String getTalie() {
        return Talie;
    }

    public void setTalie(String talie) {
        Talie = talie;
    }

    public String getRasa() {
        return Rasa;
    }

    public void setRasa(String rasa) {
        Rasa = rasa;
    }


}

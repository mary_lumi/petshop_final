package com.example.petshop;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@Controller
@RequestMapping("/petshop")
public class PetshopController {

    @Autowired
    private PetshopRepository repository;

    @RequestMapping("/search/{name}")
    @ResponseBody
    public String hello(@PathVariable("name") String name) {
        return repository.findShops(name);
    }

    @RequestMapping("")
    public String getSearchPage(Model model) {
        List<String> magazine =repository.findMagazinfactmax();
        model.addAttribute("magazine",magazine);

        List<String> angajati =repository.findangajatlenes();
        model.addAttribute("angajati", angajati);

        List<String> mancare = repository.findhranascumpa();
        model.addAttribute("mancare", mancare);

        List<String> clienti = repository.findclientfidel();
        model.addAttribute("clienti",clienti);

        List<String> stoc = repository.findstocminim();
        model.addAttribute("stoc",stoc);

        List<Salariu> salarii = repository.findsalariumediu();
        model.addAttribute("salarii",salarii);

        return "welcome";
    }

    // model = html

    @RequestMapping("/listmagazine")
    public String getAllShops(Model model) {
        List<Magazin> shops = repository.findAllShops();

        model.addAttribute("shops", shops);

        return "listmagazine";
    }
    @RequestMapping("/listangajati")
    public String getAllAgajati(Model model) {
        List<Angajat> angajati = repository.findAllAngajat();

        model.addAttribute("angajati", angajati);

        return "listangajati";
    }
    @RequestMapping("/listfactura")
    public String getAllFactura(Model model) {
        List<Factura> facturi = repository.findAllFactura();

        model.addAttribute("facturi", facturi);

        return "listfactura";
    }

    @RequestMapping("/listanimale")
    public String getAllAnimale(Model model) {

        List<Animal> animale = repository.findAllAnimal();

        model.addAttribute("animale", animale);

        return "listanimale";
    }

    @RequestMapping("/listclient")
    public String getAllClienti(Model model) {

        List<Client> clienti = repository.findAllClient();

        model.addAttribute("clienti", clienti);

        return "listclient";
    }

    @RequestMapping("/listhrana")
    public String getAllHrana(Model model) {

        List<Hrana> mancare = repository.findAllHrana();

        model.addAttribute("mancare", mancare);

        return "listhrana";
    }
    @RequestMapping("/listimportator")
    public String getAllImportator(Model model) {

        List<Importator> importatori = repository.findAllImportator();

        model.addAttribute("importatori", importatori);

        return "listimportator";
    }

    @RequestMapping(value = "/editShop", method = RequestMethod.GET)
    public String addShop(@RequestParam Integer shopId, Model model) {
        Magazin Magazin = repository.findShopById(shopId);
        model.addAttribute("shop", Magazin);

        return "editShop";
    }
    @RequestMapping(value = "/editShopAction", method = RequestMethod.POST)
    public String editShop(@ModelAttribute Magazin shop, BindingResult errors, Model model) throws SQLException {
        repository.editShop(shop);

        return "redirect:listmagazine";
    }
    @RequestMapping(value = "/editAngajat", method = RequestMethod.GET)
    public String addAngajat(@RequestParam Integer angajatId, Model model) {
        Angajat Angajat = repository.findAngajatById(angajatId);
        model.addAttribute("angajat", Angajat);

        return "editAngajat";
    }
    @RequestMapping(value = "/editAngajatAction", method = RequestMethod.POST)
    public String editAngajat(@ModelAttribute Angajat Angajat, BindingResult errors, Model model) throws SQLException {
        repository.editAngajat(Angajat);

        return "redirect:listangajati";
    }
    @RequestMapping(value = "/editAnimal", method = RequestMethod.GET)
    public String addAnimal(@RequestParam Integer animalId, Model model) {
        Animal Animal = repository.findAnimalById(animalId);
        model.addAttribute("animal", Animal);

        return "editAnimal";
    }
    @RequestMapping(value = "/editAnimalAction", method = RequestMethod.POST)
    public String editAnimal(@ModelAttribute Animal Animal, BindingResult errors, Model model) throws SQLException {
        repository.editAnimal(Animal);

        return "redirect:listanimale";
    }
    @RequestMapping(value = "/editClient", method = RequestMethod.GET)
    public String addClient(@RequestParam Integer clientId, Model model) {
        Client Client = repository.findClientById(clientId);
        model.addAttribute("client", Client);

        return "editClient";
    }
    @RequestMapping(value = "/editClientAction", method = RequestMethod.POST)
    public String editClient(@ModelAttribute Client Client, BindingResult errors, Model model) throws SQLException {
        repository.editClient(Client);

        return "redirect:listclient";
    }
    @RequestMapping(value = "/editHrana", method = RequestMethod.GET)
    public String addHrana(@RequestParam Integer hranaId, Model model) {
        Hrana Hrana = repository.findHranaById(hranaId);
        model.addAttribute("hrana", Hrana);
        List<Importator> Importators = repository.findAllImportator();
        model.addAttribute("importatori", Importators);

        return "editHrana";
    }
    @RequestMapping(value = "/editHranaAction", method = RequestMethod.POST)
    public String editHrana(@ModelAttribute Hrana Hrana, BindingResult errors, Model model) throws SQLException {
        repository.editHrana(Hrana);

        return "redirect:listhrana";
    }
    @RequestMapping(value = "/editImportator", method = RequestMethod.GET)
    public String addImportator(@RequestParam Integer importatorId, Model model) {
        Importator Importator = repository.findImportatorById(importatorId);
        model.addAttribute("importator", Importator);

        return "editImportator";
    }
    @RequestMapping(value = "/editImportatorAction", method = RequestMethod.POST)
    public String editImportator(@ModelAttribute Importator Importator, BindingResult errors, Model model) throws SQLException {
        repository.editImportator(Importator);

        return "redirect:listimportator";
    }

    @RequestMapping(value = "addShop", method = RequestMethod.GET)
    public String getAddShopPage(Model model) {
        model.addAttribute("shop", new Magazin());

        return "addShop";
    }

    // http://www.baeldung.com/spring-mvc-form-tutorial
    @RequestMapping(value = "addShop", method = RequestMethod.POST)
    public String addShop(@ModelAttribute Magazin shop, BindingResult errors, Model model) throws SQLException {
        boolean success = repository.addShop(shop);
        return "redirect:listmagazine";
    }
    @RequestMapping(value = "addAngajat", method = RequestMethod.GET)
    public String getAddAngajatPage(Model model) {
        model.addAttribute("angajat", new Angajat());

        // lista cu toate magazinele pentru UI
        List<Magazin> shops = repository.findAllShops();
        model.addAttribute("shops", shops);

        return "addAngajat";
    }
    @RequestMapping(value = "addAngajat", method = RequestMethod.POST)
    public String addAngajat(@ModelAttribute Angajat Angajat, BindingResult errors, Model model) throws SQLException {
        boolean success = repository.addAngajat(Angajat);
        return "redirect:listangajati";
    }

    @RequestMapping(value = "addAnimal", method = RequestMethod.GET)
    public String getAddAnimalPage(Model model) {
        model.addAttribute("animal", new Animal());

        return "addAnimal";
    }
    @RequestMapping(value = "addAnimal", method = RequestMethod.POST)
    public String addAnimal(@ModelAttribute Animal Animal, BindingResult errors, Model model) throws SQLException {
        boolean success = repository.addAnimal(Animal);
        return "redirect:listanimale";
    }

    @RequestMapping(value = "addClient", method = RequestMethod.GET)
    public String getAddClientPage(Model model) {
        model.addAttribute("client", new Client());

        return "addClient";
    }
    @RequestMapping(value = "addClient", method = RequestMethod.POST)
    public String addClient(@ModelAttribute Client clienti, BindingResult errors, Model model) throws SQLException {
        boolean success = repository.addClient(clienti);
        return "redirect:listclient";
    }

    @RequestMapping(value = "addHrana", method = RequestMethod.GET)
    public String getAddHranaPage(Model model) {
        model.addAttribute("hrana", new Hrana());
        List<Importator> Importators = repository.findAllImportator();
        model.addAttribute("importatori", Importators);

        return "addHrana";
    }
    @RequestMapping(value = "addHrana", method = RequestMethod.POST)
    public String addHrana(@ModelAttribute Hrana fh, BindingResult errors, Model model) throws SQLException {
        boolean success = repository.addHrana(fh);
        return "redirect:listhrana";
    }
    @RequestMapping(value = "addImportator", method = RequestMethod.GET)
    public String getAddImportatorPage(Model model) {
        model.addAttribute("imporator", new Importator());

        return "addImportator";
    }
    @RequestMapping(value = "addImportator", method = RequestMethod.POST)
    public String addImportator(@ModelAttribute Importator imp, BindingResult errors, Model model) throws SQLException {
        boolean success = repository.addImportator(imp);
        return "redirect:listimportator";
    }

    @RequestMapping(value = "deleteShop", method = RequestMethod.GET)
    public String getDeleteShopPage(Model model) {
        model.addAttribute("shop", new Magazin());

        return "deleteShop";
    }

    // http://www.baeldung.com/spring-mvc-form-tutorial
    // sterge un singur magazin dupa nume
    @RequestMapping(value = "/deleteShop", method = RequestMethod.POST)
    public String deleteShop(@RequestParam Integer shopId) {
//        System.out.println("Stergere: " + shop.getID_Magazin() + " " + shop.getNume());

        try {
            repository.deleteMagazin(shopId, "");
        } catch (SQLException e) {
            System.out.println("Eroare la stergere magazin " + e.getMessage());
        }

        return "redirect:listmagazine";
    }

    @RequestMapping("/welcome/{name}")
    public String welcome(Model model, @PathVariable("name") String name) {

        String shop = repository.findShops(name);

        model.addAttribute("name", shop);

        return "welcome";
    }

    @RequestMapping(value = "deleteAngajat", method = RequestMethod.GET)
    public String getDeleteAngajatPage(Model model) {
        model.addAttribute("angajati", new Angajat());

        return "deleteAngajat";
    }

    @RequestMapping(value = "/deleteAngajat", method = RequestMethod.POST)
    public String deleteAngajat(@RequestParam Integer angajatID) {
        try {
            repository.deleteAngajat(angajatID, "");
        } catch (SQLException e) {
            System.out.println("Eroare la stergere Angajat " + e.getMessage());
        }

        return "redirect:listangajati";
    }
    @RequestMapping(value = "deleteAnimal", method = RequestMethod.GET)
    public String getDeleteAnimalPage(Model model) {
        model.addAttribute("animal", new Angajat());

        return "deleteAnimal";
    }

    @RequestMapping(value = "/deleteAnimal", method = RequestMethod.POST)
    public String deleteAnimal(@RequestParam Integer animalID) {
        try {
            repository.deleteAnimal(animalID, "");
        } catch (SQLException e) {
            System.out.println("Eroare la stergere Animal " + e.getMessage());
        }

        return "redirect:listanimale";
    }
    @RequestMapping(value = "deleteClient", method = RequestMethod.GET)
    public String getdeleteClientPage(Model model) {
        model.addAttribute("client", new Client());

        return "deleteClient";
    }

    @RequestMapping(value = "/deleteClient", method = RequestMethod.POST)
    public String deleteClient(@RequestParam Integer clientID) {
        try {
            repository.deleteClient(clientID, "");
        } catch (SQLException e) {
            System.out.println("Eroare la stergere Client " + e.getMessage());
        }

        return "redirect:listclient";
    }
    @RequestMapping(value = "deleteHrana", method = RequestMethod.GET)
    public String getdeleteHranaPage(Model model) {
        model.addAttribute("hrana", new Hrana());

        return "deleteHrana";
    }

    @RequestMapping(value = "/deleteHrana", method = RequestMethod.POST)
    public String deleteHrana(@RequestParam Integer hranaID) {
        try {
            repository.deleteHrana(hranaID, "");
        } catch (SQLException e) {
            System.out.println("Eroare la stergere hrana " + e.getMessage());
        }

        return "redirect:listhrana";
    }
    @RequestMapping(value = "deleteImportator", method = RequestMethod.GET)
    public String getdeleteImportatorPage(Model model) {
        model.addAttribute("importator", new Importator());

        return "deleteImportator";
    }

    @RequestMapping(value = "/deleteImportator", method = RequestMethod.POST)
    public String deleteImportator(@RequestParam Integer importatorID) {
        try {
            repository.deleteImportator(importatorID, "");
        } catch (SQLException e) {
            System.out.println("Eroare la stergere importator " + e.getMessage());
        }

        return "redirect:listimportator";
    }





}

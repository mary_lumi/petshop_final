package com.example.petshop;

public class Factura {
    private Integer ID_Factura;
    private Integer ID_Client;
    private Integer ID_Angajat;
    private String Data;
    private Integer Total;
    private Integer ID_Magazin;
    private com.example.petshop.Angajat Angajat;
    private com.example.petshop.Client Client;
    private com.example.petshop.Magazin Magazin;

    public com.example.petshop.Angajat getAngajat() {
        return Angajat;
    }

    public void setAngajat(com.example.petshop.Angajat Angajat) {
        Angajat = Angajat;
    }

    public com.example.petshop.Client getClient() {
        return Client;
    }

    public void setClient(com.example.petshop.Client Client) {
        Client = Client;
    }

    public com.example.petshop.Magazin getMagazin() {
        return Magazin;
    }

    public void setMagazin(com.example.petshop.Magazin Magazin) {
        Magazin = Magazin;
    }

    public Integer getID_Factura() {
        return ID_Factura;
    }

    public void setID_Factura(Integer ID_Factura) {
        this.ID_Factura = ID_Factura;
    }

    public Integer getID_Client() {
        return ID_Client;
    }

    public void setID_Client(Integer ID_Client) {
        this.ID_Client = ID_Client;
    }

    public Integer getID_Angajat() {
        return ID_Angajat;
    }

    public void setID_Angajat(Integer ID_Angajat) {
        this.ID_Angajat = ID_Angajat;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    public Integer getTotal() {
        return Total;
    }

    public void setTotal(Integer total) {
        Total = total;
    }

    public Integer getID_Magazin() {
        return ID_Magazin;
    }

    public void setID_Magazin(Integer ID_Magazin) {
        this.ID_Magazin = ID_Magazin;
    }
}

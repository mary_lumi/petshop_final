package com.example.petshop;
import javax.persistence.*;

@Entity
@Table("angajat")
public class Angajat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer ID_Angajat;
    @Column
    private String Nume;
    @Column
    private String Prenume;
    private String Adresa;
    private String CNP;
    private Integer Salariu;
    private Integer ID_Magazin;

    public Magazin getMagazin() {
        return Magazin;
    }

    public void setMagazin(Magazin Magazin) {
        this.Magazin = Magazin;
    }

    @Transient
    private Magazin Magazin;

    public Integer getID_Angajat() {
        return ID_Angajat;
    }

    public void setID_Angajat(Integer ID_Angajat) {
        this.ID_Angajat = ID_Angajat;
    }

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public String getPrenume() {
        return Prenume;
    }

    public void setPrenume(String prenume) {
        Prenume = prenume;
    }

    public String getAdresa() {
        return Adresa;
    }

    public void setAdresa(String adresa) {
        Adresa = adresa;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public Integer getSalariu() {
        return Salariu;
    }

    public void setSalariu(Integer salariu) {
        Salariu = salariu;
    }

    public Integer getID_Magazin() {
        return ID_Magazin;
    }

    public void setID_Magazin(Integer ID_Magazin) {
        this.ID_Magazin = ID_Magazin;
    }

}

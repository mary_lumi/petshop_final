package com.example.petshop;


import javax.persistence.*;

@Entity
@Table(name = "magazin")
public class Magazin {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer ID_Magazin;
    @Column
    private String Nume;
    private String Adresa;
    private String Orar;

    public Integer getID_Magazin() {
        return ID_Magazin;
    }

    public void setID_Magazin(Integer ID_Magazin) {
        this.ID_Magazin = ID_Magazin;
    }

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public String getAdresa() {
        return Adresa;
    }

    public void setAdresa(String adresa) {
        Adresa = adresa;
    }

    public String getOrar() {
        return Orar;
    }

    public void setOrar(String orar) {
        Orar = orar;
    }
}

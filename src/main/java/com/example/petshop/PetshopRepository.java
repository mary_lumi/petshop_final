package com.example.petshop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Repository
public class PetshopRepository {

    @Autowired
    private DataSource dataSource;

    //https://www.mkyong.com/jdbc/jdbc-preparestatement-example-insert-a-record/
    public boolean addShop(Magazin shop) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "insert into magazin (Nume, Adresa, Orar) values (?, ?, ?)";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, shop.getNume());
            preparedStatement.setString(2, shop.getAdresa());
            preparedStatement.setString(3, shop.getOrar());

            success = preparedStatement.execute();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }

    public boolean addClient(Client Client) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "insert into Client (Nume, Prenume, Adresa, CNP) values (?, ?, ?, ?)";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Client.getNume());
            preparedStatement.setString(2, Client.getPrenume());
            preparedStatement.setString(3, Client.getAdresa());
            preparedStatement.setString(4, Client.getCNP());

            success = preparedStatement.execute();


        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }

    public boolean addAngajat(Angajat Angajat) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "insert into Angajat (ID_Magazin, Nume, Prenume, Adresa, CNP, Salariu) values (?, ?, ?, ?, ?, ?)";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, Angajat.getID_Magazin());
            preparedStatement.setString(2, Angajat.getNume());
            preparedStatement.setString(3, Angajat.getPrenume());
            preparedStatement.setString(4, Angajat.getAdresa());
            preparedStatement.setString(5, Angajat.getCNP());
            preparedStatement.setInt(6, Angajat.getSalariu());

            success = preparedStatement.execute();


        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }

    public boolean addAnimal(Animal Animal) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "insert into Animal (Nume, Talie, Rasa) values (?, ?, ?)";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Animal.getNume());
            preparedStatement.setString(2, Animal.getTalie());
            preparedStatement.setString(3, Animal.getRasa());

            success = preparedStatement.execute();


        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }

    public boolean addHrana(Hrana Hrana) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "insert into hrana (Nume, Tip, ID_Provizor, Pret) values ( ?, ?, ?, ?)";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Hrana.getNume());
            preparedStatement.setString(2, Hrana.getTip());
            preparedStatement.setInt(3, Hrana.getID_Provizor());
            preparedStatement.setInt(4, Hrana.getPret());

            success = preparedStatement.execute();


        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }

    public boolean addImportator(Importator Importator) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "insert into importator (Nume, Adresa, ID_Contract) values ( ?, ?, ?)";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Importator.getNume());
            preparedStatement.setString(2, Importator.getAdresa());
            preparedStatement.setInt(3, Importator.getID_contract());

            success = preparedStatement.execute();


        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }


    public void deleteAngajat(Integer angajatID, String nume) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String deleteSQL = "DELETE FROM Angajat WHERE ID_Angajat = ?";
        try {

            preparedStatement = connection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, angajatID);

            // execute delete SQL stetement
            preparedStatement.executeUpdate();

            System.out.println("Record is deleted!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (connection != null) {
                connection.close();
            }
        }
    }

    public void deleteAnimal(Integer animalId, String nume) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String deleteSQL = "DELETE FROM Animal WHERE ID_Animal = ?";
        try {

            preparedStatement = connection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, animalId);

            // execute delete SQL stetement
            preparedStatement.executeUpdate();

            System.out.println("Record is deleted!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (connection != null) {
                connection.close();
            }
        }
    }

    public void deleteClient(Integer clientId, String nume) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String deleteSQL = "DELETE FROM Client WHERE ID_Client = ?";
        try {

            preparedStatement = connection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, clientId);

            // execute delete SQL stetement
            preparedStatement.executeUpdate();

            System.out.println("Record is deleted!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (connection != null) {
                connection.close();
            }
        }
    }

    public void deleteImportator(Integer importatorId, String nume) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String deleteSQL = "DELETE FROM importator WHERE ID_Importator = ?";
        try {

            preparedStatement = connection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, importatorId);

            // execute delete SQL stetement
            preparedStatement.executeUpdate();

            System.out.println("Record is deleted!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (connection != null) {
                connection.close();
            }
        }
    }

    public void deleteMagazin(Integer shopId, String name) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String deleteSQL = "DELETE FROM magazin WHERE ID_Magazin = ?";
        try {

            preparedStatement = connection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, shopId);

            // execute delete SQL stetement
            preparedStatement.executeUpdate();

            System.out.println("Record is deleted!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (connection != null) {
                connection.close();
            }
        }
    }

    public void deleteHrana(Integer hranaId, String numeHrana) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String deleteSQL = "DELETE FROM hrana WHERE id_hrana = ?";
        try {

            preparedStatement = connection.prepareStatement(deleteSQL);
            preparedStatement.setInt(1, hranaId);

            // execute delete SQL stetement
            preparedStatement.executeUpdate();

            System.out.println("Record is deleted!");

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (connection != null) {
                connection.close();
            }
        }
    }

    public Magazin findShopById(Integer shopId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        Magazin response = jdbcTemplate.query("select * from magazin where ID_Magazin = ?",
                preparedStatement -> preparedStatement.setInt(1, shopId),
                resultSet -> {
                    if (resultSet.next()) {
                        Magazin Magazin = new Magazin();
                        Magazin.setID_Magazin(resultSet.getInt("id_magazin"));
                        Magazin.setAdresa(resultSet.getString("adresa"));
                        Magazin.setNume(resultSet.getString("nume"));
                        Magazin.setOrar(resultSet.getString("orar"));

                        return Magazin;
                    }
                    return null;
                });

        return response;
    }

    public Angajat findAngajatById(Integer angajatID) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        Angajat response = jdbcTemplate.query("select * from angajat where id_angajat = ?",
                preparedStatement -> preparedStatement.setInt(1, angajatID),
                resultSet -> {
                    if (resultSet.next()) {
                        Angajat Angajat = new Angajat();
                        Angajat.setID_Magazin(resultSet.getInt("id_magazin"));
                        Angajat.setID_Angajat(resultSet.getInt("id_angajat"));
                        Angajat.setAdresa(resultSet.getString("adresa"));
                        Angajat.setNume(resultSet.getString("nume"));
                        Angajat.setPrenume(resultSet.getString("prenume"));
                        Angajat.setCNP(resultSet.getString("CNP"));
                        Angajat.setSalariu(resultSet.getInt("salariu"));

                        return Angajat;
                    }
                    return null;
                });

        return response;
    }
    public Animal findAnimalById(Integer animalID) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        Animal response = jdbcTemplate.query("select * from animal where id_animal = ?",
                preparedStatement -> preparedStatement.setInt(1, animalID),
                resultSet -> {
                    if (resultSet.next()) {
                        Animal Animal = new Animal();
                        Animal.setID_Animal(resultSet.getInt("id_animal"));
                        Animal.setNume(resultSet.getString("nume"));
                        Animal.setRasa(resultSet.getString("rasa"));
                        Animal.setTalie(resultSet.getString("talie"));


                        return Animal;
                    }
                    return null;
                });

        return response;
    }
    public Client findClientById(Integer clientID) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        Client response = jdbcTemplate.query("select * from client where id_client = ?",
                preparedStatement -> preparedStatement.setInt(1, clientID),
                resultSet -> {
                    if (resultSet.next()) {
                        Client Client = new Client();
                        Client.setID_Client(resultSet.getInt("id_client"));
                        Client.setCNP(resultSet.getString("CNP"));
                        Client.setNume(resultSet.getString("nume"));
                        Client.setPrenume(resultSet.getString("prenume"));
                        Client.setAdresa(resultSet.getString("adresa"));

                        return Client;
                    }
                    return null;
                });

        return response;
    }
    public Hrana findHranaById(Integer hranaID) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        Hrana response = jdbcTemplate.query("select * from hrana where id_hrana = ?",
                preparedStatement -> preparedStatement.setInt(1, hranaID),
                resultSet -> {
                    if (resultSet.next()) {
                        Hrana Hrana = new Hrana();
                        Hrana.setID_Hrana(resultSet.getInt("id_hrana"));
                        Hrana.setID_Provizor(resultSet.getInt("id_provizor"));
                        Hrana.setTip(resultSet.getString("tip"));
                        Hrana.setNume(resultSet.getString("nume"));
                        Hrana.setPret(resultSet.getInt("pret"));

                        return Hrana;
                    }
                    return null;
                });

        return response;
    }
    public Importator findImportatorById(Integer importatorID) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        Importator response = jdbcTemplate.query("select * from importator where id_importator = ?",
                preparedStatement -> preparedStatement.setInt(1, importatorID),
                resultSet -> {
                    if (resultSet.next()) {
                        Importator Importator = new Importator();
                        Importator.setID_importator(resultSet.getInt("id_importator"));
                        Importator.setID_contract(resultSet.getInt("id_contract"));
                        Importator.setAdresa(resultSet.getString("adresa"));
                        Importator.setNume(resultSet.getString("nume"));

                        return Importator;
                    }
                    return null;
                });

        return response;
    }


    public boolean editShop(Magazin Magazin) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "update magazin set `adresa`=?, `nume`=?, `orar` = ? where id_magazin=?";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Magazin.getAdresa());
            preparedStatement.setString(2, Magazin.getNume());
            preparedStatement.setString(3, Magazin.getOrar());
            preparedStatement.setInt(4, Magazin.getID_Magazin());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }

    public boolean editAngajat(Angajat Angajat) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "update Angajat set  `adresa`=?, `nume`=?, `prenume` = ?, `CNP` = ?, `salariu` = ? where id_angajat = ?";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Angajat.getAdresa());
            preparedStatement.setString(2, Angajat.getNume());
            preparedStatement.setString(3, Angajat.getPrenume());
            preparedStatement.setString(4, Angajat.getCNP());
            preparedStatement.setInt(5, Angajat.getSalariu());
            preparedStatement.setInt(6, Angajat.getID_Angajat());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }

    public boolean editClient(Client Client) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "update Client set `adresa`=?, `nume`=?, `prenume` = ?, `CNP`= ? where id_client = ?";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Client.getAdresa());
            preparedStatement.setString(2, Client.getNume());
            preparedStatement.setString(3, Client.getPrenume());
            preparedStatement.setString(4, Client.getCNP());
            preparedStatement.setInt(5, Client.getID_Client());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }

    public boolean editAnimal(Animal Animal) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "update Animal set `rasa`=?, `nume`=?, `talie` = ? where id_animal = ?";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Animal.getRasa());
            preparedStatement.setString(2, Animal.getNume());
            preparedStatement.setString(3, Animal.getTalie());
            preparedStatement.setInt(4, Animal.getID_Animal());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }
    public boolean editHrana(Hrana Hrana) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "update hrana set `tip`=?, `nume`=?, `pret`=?, `id_provizor` = ? where id_hrana = ?";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Hrana.getTip());
            preparedStatement.setString(2, Hrana.getNume());
            preparedStatement.setInt(3, Hrana.getPret());
            preparedStatement.setInt(4, Hrana.getID_Provizor());
            preparedStatement.setInt(5, Hrana.getID_Hrana());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }

    public boolean editImportator(Importator Importator) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "update importator set `adresa`=?, `nume`=?, `id_contract`=? where id_importator = ?";

        boolean success = false;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, Importator.getAdresa());
            preparedStatement.setString(2, Importator.getNume());
            preparedStatement.setInt(3, Importator.getID_contract());
            preparedStatement.setInt(4, Importator.getID_importator());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return success;
    }


    //https://www.mkyong.com/spring/spring-jdbctemplate-querying-examples/
    public List<Magazin> findAllShops() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from magazin");

        List<Magazin> shops = new ArrayList<>();

        for (Map row : rows) {
            Magazin shop = new Magazin();
            shop.setID_Magazin((Integer) row.get("ID_Magazin"));
            shop.setNume((String) row.get("Nume"));
            shop.setAdresa((String) row.get("Adresa"));
            shop.setOrar((String) row.get("Orar"));

            shops.add(shop);
        }

        return shops;
    }

    public List<Angajat> findAllAngajat() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select a.*, m.nume as 'numeMagazin' from angajat as a, magazin as m where a.ID_magazin = m.ID_magazin");

        List<Angajat> angajati = new ArrayList<>();

        for (Map row : rows) {
            Angajat ang = new Angajat();
            ang.setID_Angajat((Integer) row.get("ID_Angajat"));
            ang.setNume((String) row.get("Nume"));
            ang.setPrenume((String) row.get("Prenume"));
            ang.setAdresa((String) row.get("Adresa"));
            ang.setCNP((String) row.get("CNP"));
            ang.setSalariu((Integer) row.get("Salariu"));

            Magazin Magazin = new Magazin();
            Magazin.setNume((String) row.get("numeMagazin"));
            ang.setMagazin(Magazin);

            angajati.add(ang);
        }

        return angajati;
    }

    public List<Client> findAllClient() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from client");

        List<Client> clienti = new ArrayList<>();

        for (Map row : rows) {
            Client cl = new Client();
            cl.setID_Client((Integer) row.get("ID_Client"));
            cl.setNume((String) row.get("Nume"));
            cl.setPrenume((String) row.get("Prenume"));
            cl.setAdresa((String) row.get("Adresa"));
            cl.setCNP((String) row.get("CNP"));

            clienti.add(cl);
        }

        return clienti;
    }

    public List<Animal> findAllAnimal() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from animal");

        List<Animal> animale = new ArrayList<>();

        for (Map row : rows) {
            Animal anim = new Animal();
            anim.setID_Animal((Integer) row.get("ID_Animal"));
            anim.setNume((String) row.get("Nume"));
            anim.setRasa((String) row.get("Rasa"));
            anim.setTalie((String) row.get("Talie"));

            animale.add(anim);
        }

        return animale;
    }

    public List<Hrana> findAllHrana() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select h.*, i.nume as 'numeImportator' from hrana as h, importator as i where h.ID_Provizor = i.ID_Importator");

        List<Hrana> Hrana = new ArrayList<>();

        for (Map row : rows) {
            Hrana hrana1 = new Hrana();
            hrana1.setID_Hrana((Integer) row.get("ID_Hrana"));
            hrana1.setNume((String) row.get("Nume"));
            hrana1.setPret((Integer) row.get("Pret"));
            hrana1.setTip((String) row.get("Tip"));
            hrana1.setID_Provizor((Integer) row.get("ID_Provizor"));

            Importator Importator = new Importator();
            Importator.setNume((String) row.get("numeImportator"));
            hrana1.setImportator(Importator);
            Hrana.add(hrana1);
        }

        return Hrana;
    }
    public List<Factura> findAllFactura() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select f.*, c.nume as 'numeClient', m.nume as 'numeMagazin', a.nume as 'numeAngajat', sum(h.pret*fh.cantitate) as 'totalcalculat' " +
                "from factura as f, client as c, magazin as m,angajat as a, fact_hrana as fh, hrana as h " +
                "where f.id_angajat = a.id_angajat and f.id_client = c.id_client and f.id_magazin = m.id_magazin AND f.id_factura = fh.id_factura AND fh.id_hrana = h.ID_Hrana GROUP BY f.id_factura");

        List<Factura> Factura = new ArrayList<>();

        for (Map row : rows) {
            Factura factura1 = new Factura();
            factura1.setID_Factura((Integer) row.get("id_factura"));
            factura1.setData((String) row.get("data"));
            factura1.setID_Client((Integer) row.get("id_client"));
            factura1.setID_Angajat((Integer) row.get("id_angajat"));
            factura1.setID_Magazin((Integer) row.get("id_magazin"));
            factura1.setTotal(((BigDecimal) row.get("totalcalculat")).intValue());


            Client Client = new Client();
            Client.setNume((String) row.get("numeClient"));
            factura1.setClient(Client);
            Angajat Angajat = new Angajat();
            Angajat.setNume((String) row.get("numeAngajat"));
            factura1.setAngajat(Angajat);
            Magazin Magazin = new Magazin();
            Magazin.setNume((String) row.get("numeMagazin"));
            factura1.setMagazin(Magazin);

            Factura.add(factura1);
        }
        return Factura;
    }


    public List<Importator> findAllImportator() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("select * from importator");

        List<Importator> importatori = new ArrayList<>();

        for (Map row : rows) {
            Importator imp = new Importator();
            imp.setID_importator((Integer) row.get("ID_Importator"));
            imp.setID_contract((Integer) row.get("ID_Contract"));
            imp.setNume((String) row.get("Nume"));
            imp.setAdresa((String) row.get("Adresa"));
            imp.setData_finalizare((String) row.get("Data_finalizare"));

            importatori.add(imp);
        }

        return importatori;
    }


    public String findShops(String shopName) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        String response = jdbcTemplate.query("select Nume from magazin where Nume like ?", new PreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement preparedStatement) throws SQLException {
                        preparedStatement.setString(1, "%" + shopName + "%");
                    }
                },
                new ResultSetExtractor<String>() {
                    @Override
                    public String extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        if (resultSet.next()) {
                            return resultSet.getString(1);
                        }
                        return null;
                    }
                });

        return response;
    }

    public List<String> findMagazinfactmax() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT M.Nume, COUNT(f.ID_Factura) AS fac  FROM  magazin AS M JOIN factura AS f ON M.ID_Magazin = f.ID_Magazin HAVING MAX(fac) ORDER BY M.nume");

        List<String> magazine = new ArrayList<String>();
        for (Map row : rows) {
            String magazin;
            magazin = (String) row.get("Nume");
            magazine.add(magazin);
        }
        return magazine;
    }//numele magazinului care are cele mai multe facturi

    public List<String> findangajatlenes() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT \n" +
                "    A.Nume\n" +
                "FROM\n" +
                "    angajat AS A\n" +
                "        JOIN\n" +
                "    factura AS f ON A.ID_Angajat = f.ID_Angajat\n" +
                "HAVING COUNT(f.ID_Factura) > ALL (SELECT \n" +
                "        COUNT(ID_Factura)\n" +
                "    FROM\n" +
                "        factura\n" +
                "    GROUP BY ID_Factura)");

        List<String> angajati = new ArrayList<String>();
        for (Map row : rows) {
            String angajat;
            angajat = (String) row.get("Nume");
            angajati.add(angajat);
        }
        return angajati;
    }//numele anlagatului care are cele mai multe facturi

    public List<String> findhranascumpa() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT A.Nume FROM  animal AS A JOIN hrana_animal AS ha ON A.ID_Animal = ha.ID_Animal JOIN hrana AS H ON H.ID_Hrana = ha.ID_Hrana HAVING MAX(H.Pret) ORDER BY A.Nume");

        List<String> animale = new ArrayList<String>();
        for (Map row : rows) {
            String animal;
            animal = (String) row.get("Nume");
            animale.add(animal);
        }
        return animale;
    }//numele animalului care are hrana cea mai scumpa

    public List<String> findclientfidel() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT C.Nume FROM client  AS C JOIN factura AS f ON C.ID_Client = f.ID_Client  HAVING SUM(F.Total) ORDER BY C.Nume");

        List<String> clienti = new ArrayList<String>();
        for (Map row : rows) {
            String client;
            client = (String) row.get("Nume");
            clienti.add(client);
        }
        return clienti;
    }//numele clientului care a cheltuit cel mai mult

    public List<String> findstocminim() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT \n" +
                "    M.Nume\n" +
                "FROM\n" +
                "    magazin AS M\n" +
                "        JOIN\n" +
                "    stoc AS S ON S.ID_Magazin = M.ID_Magazin\n" +
                "HAVING SUM(S.Cantitate) > ANY (SELECT \n" +
                "        SUM(S1.Cantitate)\n" +
                "    FROM\n" +
                "        stoc AS S1\n" +
                "    GROUP BY S1.ID_Stoc)");

        List<String> magazine = new ArrayList<String>();
        for (Map row : rows) {
            String magazin;
            magazin = (String) row.get("Nume");
            magazine.add(magazin);
        }
        return magazine;
    }//numele magazinului care are cel mai mic stoc

    public List<Salariu> findsalariumediu() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT M.Nume, AVG(A.Salariu) FROM angajat AS A JOIN magazin AS M ON A.ID_Magazin = M.ID_Magazin GROUP BY M.Nume");

        List<Salariu> magazine = new ArrayList<Salariu>();
        for (Map row : rows) {
            Salariu magazin = new Salariu();
            magazin.setNume((String) row.get("Nume"));
            magazin.setSalariu((BigDecimal) row.get("AVG(A.Salariu)"));
            magazine.add(magazin);
        }
        return magazine;
    }//numele magazinelor si salariu mediu per magazin

}


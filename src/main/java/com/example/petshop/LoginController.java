package com.example.petshop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/login")
    public String loginPage(){
        return "login";
    }

    @RequestMapping("")
    public String homePage() {
        return "redirect:/petshop";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registerPage() {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute User user) {
        User newUser = new User();

        if (!user.getPassword().equals(user.getConfirmedPassword())) {
            return "redirect:register?passDontMatch";
        }

        newUser.setUsername(user.getUsername());
        newUser.setPassword(user.getPassword());

        try {
            userRepository.save(newUser);
        } catch (Exception e) {
            return "redirect:register?sameUsername";
        }

        return "redirect:login?successfullyRegister";
    }

}

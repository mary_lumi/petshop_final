package com.example.petshop;


public class Importator {
    private Integer ID_importator;
    private String Nume;
    private String Adresa;
    private Integer ID_contract;
    private String Data_finalizare;

    public Integer getID_importator() {
        return ID_importator;
    }

    public void setID_importator(Integer ID_importator) {
        this.ID_importator = ID_importator;
    }

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public String getAdresa() {
        return Adresa;
    }

    public void setAdresa(String adresa) {
        Adresa = adresa;
    }

    public Integer getID_contract() {
        return ID_contract;
    }

    public void setID_contract(Integer ID_contract) {
        this.ID_contract = ID_contract;
    }

    public String getData_finalizare() {
        return Data_finalizare;
    }

    public void setData_finalizare(String data_finalizare) {
        Data_finalizare = data_finalizare;
    }
}

package com.example.petshop;

import java.math.BigDecimal;

public class Salariu{
    private String Nume;
    private BigDecimal Salariu;

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public BigDecimal getSalariu() {
        return Salariu;
    }

    public void setSalariu(BigDecimal salariu) {
        Salariu = salariu;
    }
}

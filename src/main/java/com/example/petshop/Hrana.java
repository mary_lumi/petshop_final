package com.example.petshop;

import javax.persistence.Transient;

public class Hrana {
    private Integer ID_Hrana;
    private String Nume;
    private String Tip;
    private Integer ID_Provizor;
    private Integer Pret;

    // pentru a nu fi luat in calcul in tranzactii
    //
    @Transient
    private Importator Importator;

    public Integer getID_Hrana() {
        return ID_Hrana;
    }

    public void setID_Hrana(Integer ID_Hrana) {
        this.ID_Hrana = ID_Hrana;
    }

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public String getTip() {
        return Tip;
    }

    public void setTip(String tip) {
        Tip = tip;
    }

    public Integer getID_Provizor() {
        return ID_Provizor;
    }

    public void setID_Provizor(Integer ID_Provizor) {
        this.ID_Provizor = ID_Provizor;
    }

    public Integer getPret() {
        return Pret;
    }

    public void setPret(Integer pret) {
        Pret = pret;
    }

    public Importator getImportator() {
        return Importator;
    }

    public void setImportator(Importator Importator) {
        this.Importator = Importator;
    }
}
